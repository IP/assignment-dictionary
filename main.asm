%include "lib.asm"
%include "words.inc"
%define MAX_SIZE 256

section .data
	message_not_found: db "Key is not in the dictionary", 10, 0
	message_too_long: db "Word should be less than 256 symbols", 10, 0


global _start

extern find_word

section .text

_start:
	sub rsp, MAX_SIZE
	mov rdi, rsp				; rdi - points at the start of the word
	mov rsi, MAX_SIZE			; rsi - size of buffer
	call read_word				; return address or 0 in rax
	cmp rax, 0				; if key is more than 256 symbols
	je .out_of_range
	mov rsi, NEXT
	mov rdi, rax				; rdi - address of the key
	call find_word				; return address or 0 in rax
	add rsp, MAX_SIZE
	cmp rax, 0
	je .key_not_found
	mov rdi, rax				; rdi - address of the key
	push rdi
	call string_length			; rax - length of the key
	pop rdi
	add rdi, rax
	inc rdi					; null-terminator
	call print_string			; print value
	mov rdi, 0
	call exit
.out_of_range:
	mov rdi, message_too_long
	mov rsi, 2
	call print_string
	call exit
.key_not_found:
	mov rdi, message_not_found
	mov rsi, 2
	call print_string
	call exit


