AS = nasm
ASFLAGS = -f elf64
LD = ld

program: lib.o dict.o main.o
	$(LD) -o program $^

main.o: main.asm colon.inc words.inc lib.asm
	$(AS) $(ASFLAGS) -o $@ $<

lib.o: lib.asm
	$(AS) $(ASFLAGS) -o $@ $<
	
dict.o: dict.asm
	$(AS) $(ASFLAGS) -o $@ $<

clean:
	rm -f *.o program

.PHONY: clean


