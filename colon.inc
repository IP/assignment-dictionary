%define NEXT 0

; Agruments:
; %1 - a pointer to a null terminated key string
; %2 - a pointer to the last word in the dictionary

%macro colon 2
	%ifid %2
		%2: dq NEXT
		%define NEXT %2
	%else
		%fatal "Identifier error"
	%endif
	%ifstr %1
		db %1, 0
	%else
		%fatal "Key Error"
	%endif
%endmacro


