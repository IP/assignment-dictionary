section .text
global find_word
extern string_equals


; Arguments:
; rsi - a pointer to the last word in the dictionary
; rdi - a pointer to a null terminated key string


find_word:
.loop:
	mov r8, rsi
	add rsi, 8
	call string_equals
	cmp rax, 0
	jne .value_found
	mov rsi, [r8]
	cmp rsi, 0
	je .value_not_found
	jmp .loop
.value_found:
    mov rax, rsi			; return address
    ret
.value_not_found:
    xor rax, rax			; return 0
    ret
    
    
