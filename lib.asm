global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy


section .text

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60				; exit syscall
    syscall


; Принимает указатель на нуль-терминированную строку, возвращает её длину
; arguments:
; rdi - pointer to null-terminated string
string_length:										
	mov rax, rdi			; address of memory, where string starts -> rax
	mov cl, [rax]			; first symbol of string puts in 8 lower bits of rcx
	test cl, cl			; sets flags (ZF, SF, PF), doesn't change operands
	jne .loop			; if ZF != 0 goes to .loop 
	jmp .end			; if ZF == 0
	
.loop:
	inc rax				; address of next symbol of string -> rax
	mov cl, [rax]			; next symbol of string puts in 8 lower bits of rcx
	test cl, cl										
	jne .loop										
	
.end:
	sub rax, rdi			; rax - rdi -> rax
	ret


; write syscall arguments:
; rax - 0x01
; rdi - type: unsigned int fd, file descriptor
; rsi - type: const char *buf, pointer to a buffer where the data is stored	
; rdx - type: size_t, count the number of bytes to write from the buffer


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
; arguments:
; rdi - pointer to null-terminated string
print_string:
	mov rsi, rdi			; rsi - pointer to a buffer where the data is stored
	call string_length		; strLen -> rax
	mov rdx, rax			; strLen -> rdx, rdx - the number of bytes to write from the buffer
	mov rax, 1			; write syscall
	mov rdi, 1			; stdout descriptor
	syscall
    ret
    

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA									; 


; Принимает код символа и выводит его в stdout
; arguments:
; rdi - code of symbol
print_char:
	mov rax, 1																
	push rdi			; puts code of symbol on stack
	mov rsi, rsp			; rsi - pointer to a code of symbol
	mov rdi, 1										
	mov rdx, 1			; size of symbol 1 byte
	syscall
	pop rdi				; restore rdi
    ret						


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
; arguments: 
; rdi - number 
print_uint:
    mov rax, rdi			; rax - number 
    mov r10, rsp			; save rsp
    push 0				; puts null-terminator in stack, rsp points at null-terminator
.loop:
	xor rdx, rdx			; reset rdx
	mov r9, 10			; divisor
	div r9 				; divide by r9, rax % r9 -> rdx
	add rdx, 48			; ASCII-code
	dec rsp				; address before null-terminator
	mov [rsp], dl 			; puts 8 lower bits of rdx at the address of rsp
	cmp rax, 0			; check if rax == 0
	jne .loop
mov rdi, rsp				; rdi - pointer to a null-terminated string
call print_string
mov rsp, r10				; restore rsp
ret
    
    
; Выводит знаковое 8-байтовое число в десятичном формате 
; arguments: 
; rdi - number
print_int:
	cmp rdi, 0
	jl .print_neg			; if rdi < 0
	jmp .end
.print_neg:
	neg rdi				; change the sign
	push rdi			; puts rdi on the stack
	mov rdi, 0x2D			; puts '-' code in rdi
	call print_char			; print minus
	pop rdi				; restore rdi
	jmp .end
.end:
	call print_uint
	ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
; arguments:
; rdi - pointer to first null-terminated string
; rsi - pointer to second null-terminated string
string_equals:
	xor rax, rax			; reset rax
	xor r8, r8			; reset r8, r8 - stores symbol of first string
	xor r9, r9			; reset r9, r9 - stores symbol of second string
	xor r10, r10			; reset r10, r10 - stores position of current symbols
.loop:
	mov r8b, [rdi + r10]		; symbol of first string -> r8
	mov r9b, [rsi + r10]		; symbol of second string -> r9
	cmp r8b, r9b			; compare symbol of first string with corresponding symbol of second string
	jne .end
	inc r10				; increase position
	cmp r8b, 0			; check for null-terminator
	jne .loop
	mov rax, 1
	ret
.end:
    ret


; read syscall arguments:
; rax - 0x00
; rdi - type: unsigned int fd, file descriptor
; rsi - type: char *buf, the buffer where the read data is to be stored	
; rdx - type: size_t count, the number of bytes to be read from the file


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	xor rax, rax
	xor rdi, rdi
	push 0				; end of the stream
	mov rsi, rsp			; rsi - points at the top of the stack, the read data will be stored there
	mov rdx, 1			; size of one symbol
	syscall
	pop rax				; read data -> rax (0 if the end of the stream)
    ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
; arguments:
; rdi - points at the start of the word
; rsi - size of buffer
read_word:
	xor r8, r8			; flag for space symbols
	xor r9, r9			; r9 - number of bytes in the word
.loop:
	push rdi			; save rdi, rsi
	push rsi
	call read_char			; symbol -> rax
	pop rsi				; restore rdi, rsi
	pop rdi											
	cmp rax, 0			; end of the stream
	je .end
	cmp rax, 0x20			; space
	je .space
	cmp rax, 0x9			; tab
	je .space
	cmp rax, 0xA			; line feed
	je .space
	mov r8, 0x1			; next space symbols will mean the end of the word
	cmp r9, rsi			; rsi - size of buffer in bytes, r9 - current size of the word in bytes
	je .outOfRange
	mov [rdi + r9], rax		; store symbol in memory
	inc r9				; increment number of bytes in the word
	jmp .loop
.space:
	cmp r8, 0			; new 
	jne .end
	jmp .loop
.outOfRange:
	xor rax, rax			; return 0
	ret
.end:
	mov rdx, r9			; size of word in bytes -> rdx
	mov rax, rdi			; address -> rax
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
; arguments:
; rdi - pointer to string
parse_uint:
	xor rax, rax
	xor rdx, rdx			; rdx - current number of digits in number
	xor r8, r8			; r8 - current symbol
.loop:
	mov r8b, [rdi + rdx]
	sub r8b, 0x30 			; ASCII
	cmp r8b, 0			; check if digit in range [0, 9]
	jnae .end			; not above or equal
	cmp r8b, 9			; check if digit in range [0, 9]
	jnbe .end			; not below or equal
	inc rdx 			; increment number of digits
	imul rax, 10			; rax * 10 -> rax
	add rax, r8			; rax + currents digit -> rax
	jmp .loop
.end:
	ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
; arguments:
; rdi - pointer to string
parse_int:
	xor r8, r8			; r8 - current digit
	mov r8b, [rdi]
	cmp r8b, 0x2D			; if '-'
	je .neg
	call parse_uint			; number is positive
	ret
.neg:
	push rdi			; save rdi
	mov rdi, 0x2D			; print '-'
	call print_char									
	pop rdi											
	inc rdi				; move to the next symbol
	call parse_uint			; now number is positive
	inc rdx				; increment rdx because of the '-'
	ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
; agruments:
; rdi - pointer to string
; rsi - pointer to buffer
; rdx - size of buffer
string_copy:
    xor rcx, rcx			; rcx - current string size
    xor r8, r8				; r8 - current symbol
.loop:
    mov r8b, [rdi + rcx]
    mov [rsi + rcx], r8b		; writes current symbol in buffer
    cmp rcx, rdx			; rcx - current string size, rdx - size of buffer
    je .outOfRange
    cmp r8, 0				; null-terminator
    je .end
    inc rcx				; increment current string size
    jmp .loop
.outOfRange:
	xor rax, rax			; return 0
	ret
.end:
	mov rcx, rax
	ret


